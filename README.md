The project contains examples of using PHP solutions in the field of machine learning for language detection of a text document. Solutions using PHP extension FANN, PHP-ML library, Rubix ML library, as well as a combination of PHP and Python are considered. The project consists of several sub-projects, each located in its own directory.

## Requirements

First install [Docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/) if not already done.

## Usage
Change into the sub-project directory from the project root and run the following commands from the terminal.
```bash
$ make init
$ docker-compose run --rm php-cli php src/train.php    # for pure PHP projects 
$ docker-compose run --rm php-cli python3 src/train.py # for PHP+Python projects
$ docker-compose run --rm php-cli php src/predict.php
```
