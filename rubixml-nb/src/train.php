<?php

use Rubix\ML\Classifiers\GaussianNB;
use Rubix\ML\CrossValidation\Metrics\Accuracy;
use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Extractors\CSV;
use Rubix\ML\Other\Tokenizers\Word;
use Rubix\ML\PersistentModel;
use Rubix\ML\Persisters\Filesystem;
use Rubix\ML\Pipeline;
use Rubix\ML\Transformers\TfIdfTransformer;
use Rubix\ML\Transformers\WordCountVectorizer;

ini_set('memory_limit', '-1');

include __DIR__.'/../vendor/autoload.php';

// Prepare data
$dataset              = Labeled::fromIterator(new CSV(__DIR__.'/../data/dataset.csv', true));
[$training, $testing] = $dataset->stratifiedSplit(0.9);

// Build the model
$model    = new GaussianNB();
$pipeline = new PersistentModel(
    new Pipeline(
        [
            new WordCountVectorizer(10000, 1, PHP_INT_MAX, new Word()),
            new TfIdfTransformer(),
        ],
        $model
    ),
    new Filesystem(__DIR__.'/../model/pipeline')
);

// Train the model using training data
$pipeline->train($training);

// Evaluate the model using testing data
$predictedLabels = $pipeline->predict($testing);
$metric          = new Accuracy();
$accuracy        = $metric->score($predictedLabels, $testing->labels());
printf("Accuracy: %.1f%%\n", $accuracy * 100.0);

// Persist the model together with data transformers
$pipeline->save();
