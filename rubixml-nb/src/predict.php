<?php

use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Persisters\Filesystem;

ini_set('memory_limit', '-1');

include __DIR__.'/../vendor/autoload.php';

// Load trained model together with data transformers
$persister = new Filesystem(__DIR__.'/../model/pipeline');
$pipeline  = $persister->load();

// Retrieve text sample
while (empty($text)) $text = readline("Enter some text to analyze: \n");

// Predict text language
$predicted = $pipeline->predict(Unlabeled::build([$text]));
printf("Predicted language: %s\n", $predicted[0]);
