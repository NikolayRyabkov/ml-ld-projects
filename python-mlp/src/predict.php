<?php

ini_set('memory_limit', '-1');

// Retrieve text sample
while (empty($text)) $text = readline("Enter some text to analyze: \n");

// Predict text language
$cmd       = sprintf("python3 %s/predict.py --text '%s'", __DIR__, $text);
$predicted = shell_exec($cmd);
printf("Predicted language: %s", $predicted);
