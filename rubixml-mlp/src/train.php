<?php

use Rubix\ML\Classifiers\MultilayerPerceptron;
use Rubix\ML\CrossValidation\Metrics\Accuracy;
use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Extractors\CSV;
use Rubix\ML\NeuralNet\ActivationFunctions\Sigmoid;
use Rubix\ML\NeuralNet\Layers\Activation;
use Rubix\ML\NeuralNet\Layers\Dense;
use Rubix\ML\NeuralNet\Optimizers\Stochastic;
use Rubix\ML\Other\Loggers\Screen;
use Rubix\ML\Other\Tokenizers\Word;
use Rubix\ML\PersistentModel;
use Rubix\ML\Persisters\Filesystem;
use Rubix\ML\Pipeline;
use Rubix\ML\Transformers\TfIdfTransformer;
use Rubix\ML\Transformers\WordCountVectorizer;

ini_set('memory_limit', '-1');

include __DIR__.'/../vendor/autoload.php';

// Prepare data
$dataset              = Labeled::fromIterator(new CSV(__DIR__.'/../data/dataset.csv', true));
[$training, $testing] = $dataset->stratifiedSplit(0.9);

// Build the model
$model = new MultilayerPerceptron(
    [new Dense(5), new Activation(new Sigmoid())],      // hidden layer
    1,                                                  // batch size
    new Stochastic(0.01)                                // optimizer and learning rate
);
$model->setLogger(new Screen());
$pipeline  = new PersistentModel(
    new Pipeline(
        [
            new WordCountVectorizer(10000, 1, PHP_INT_MAX, new Word()),
            new TfIdfTransformer(),
        ],
        $model
    ),
    new Filesystem(__DIR__.'/../model/pipeline')
);

// Train the model using training data
$pipeline->train($training);

// Evaluate the model using testing data
$predictedLabels = $pipeline->predict($testing);
$metric          = new Accuracy();
$accuracy        = $metric->score($predictedLabels, $testing->labels());
printf("Accuracy: %.1f%%\n", $accuracy * 100.0);

// Persist the model together with data transformers
$pipeline->save();
