import argparse
import os
from joblib import load

dir = os.path.dirname(os.path.realpath(__file__))

# Parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--text')
args = parser.parse_args()

# Load trained model together with data transformers
pipeline = load(dir + '/../model/pipeline.joblib')

# Retrieve text sample
text = args.text

# Predict text language
predicted = pipeline.predict([text])
print(predicted[0])
