import os
import pandas as pd
from joblib import dump
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline

# Prepare data
dir  = os.path.dirname(os.path.realpath(__file__))
data = pd.read_csv(dir + '/../data/dataset.csv')
X    = data['Text']
y    = data['language']
X_train, X_test, y_train, y_test = train_test_split(X, y, stratify = y, train_size = 0.9)

# Build the model
model    = MultinomialNB()
pipeline = make_pipeline(
    CountVectorizer(analyzer = 'word', max_features = 10000),
    TfidfTransformer(),
    model
)

# Train the model using training data
pipeline.fit(X_train, y_train)

# Evaluate the model using testing data
predicted_labels = pipeline.predict(X_test)
print("Accuracy: {0:.1f}%".format(accuracy_score(y_test, predicted_labels) * 100.0))

# Persist the model together with data transformers
dump(pipeline, dir + '/../model/pipeline.joblib')
