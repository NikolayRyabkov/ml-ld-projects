<?php

use Phpml\Classification\MLPClassifier;
use Phpml\CrossValidation\StratifiedRandomSplit;
use Phpml\Dataset\ArrayDataset;
use Phpml\Dataset\CsvDataset;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Metric\Accuracy;
use Phpml\ModelManager;
use Phpml\NeuralNetwork\ActivationFunction\Sigmoid;
use Phpml\NeuralNetwork\Layer;
use Phpml\NeuralNetwork\Node\Neuron;
use Phpml\Tokenization\WordTokenizer;
use PhpmlMlpExamples\FeatureExtraction\TokenCountVectorizer;

ini_set('memory_limit', '-1');

include __DIR__.'/../vendor/autoload.php';

// Prepare data
$dataset = new CsvDataset(__DIR__.'/../data/dataset.csv', 1);
$samples = [];
foreach ($dataset->getSamples() as $sample) {
    $samples[] = $sample[0];
}
$vectorizer  = new TokenCountVectorizer(new WordTokenizer());
$vectorizer->fit($samples);
$vectorizer->transform($samples);
$transformer = new TfIdfTransformer();
$transformer->fit($samples);
$transformer->transform($samples);
$dataset     = new ArrayDataset($samples, $dataset->getTargets());
$randomSplit = new StratifiedRandomSplit($dataset, 0.1);

// Build the model
$model = new MLPClassifier(
    count($vectorizer->getVocabulary()),                // input layer size
    [new Layer(5, Neuron::class, new Sigmoid())],       // hidden layers
    array_unique($randomSplit->getTrainLabels()),       // classes
    100,                                                // iteration count
    null,                                               // default activation function
    0.01                                                // learning rate
);

// Train the model using training data
$model->train($randomSplit->getTrainSamples(), $randomSplit->getTrainLabels());

// Evaluate the model using testing data
$predictedLabels = $model->predict($randomSplit->getTestSamples());
$accuracy        = Accuracy::score($randomSplit->getTestLabels(), $predictedLabels);
printf("Accuracy: %.1f%%\n", $accuracy * 100.0);

// Persist the model and data transformers
$modelManager = new ModelManager();
$modelManager->saveToFile($model, __DIR__.'/../model/model');
file_put_contents(__DIR__.'/../model/vectorizer', serialize($vectorizer));
file_put_contents(__DIR__.'/../model/transformer', serialize($transformer));
