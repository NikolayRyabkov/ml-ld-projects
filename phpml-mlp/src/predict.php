<?php

use Phpml\ModelManager;

ini_set('memory_limit', '-1');

include __DIR__.'/../vendor/autoload.php';

// Load trained model and data transformers
$modelManager = new ModelManager();
$model        = $modelManager->restoreFromFile(__DIR__.'/../model/model');
$vectorizer   = unserialize(file_get_contents(__DIR__.'/../model/vectorizer'));
$transformer  = unserialize(file_get_contents(__DIR__.'/../model/transformer'));

// Retrieve text sample
while (empty($text)) $text = readline("Enter some text to analyze: \n");

// Apply data transformers
$textAsArray = [$text];
$vectorizer->transform($textAsArray);
$transformer->transform($textAsArray);

// Predict text language
$predicted = $model->predict($textAsArray);
printf("Predicted language: %s\n", $predicted[0]);
