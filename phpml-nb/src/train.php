<?php

use Phpml\Classification\NaiveBayes;
use Phpml\CrossValidation\StratifiedRandomSplit;
use Phpml\Dataset\CsvDataset;
use Phpml\Dataset\ArrayDataset;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Metric\Accuracy;
use Phpml\ModelManager;
use Phpml\Pipeline;
use Phpml\Tokenization\WordTokenizer;
use PhpmlNbExamples\FeatureExtraction\TokenCountVectorizer;

ini_set('memory_limit', '-1');

include __DIR__.'/../vendor/autoload.php';

// Prepare data
$dataset = new CsvDataset(__DIR__.'/../data/dataset.csv', 1);
$samples = [];
foreach ($dataset->getSamples() as $sample) {
    $samples[] = $sample[0];
}
$dataset     = new ArrayDataset($samples, $dataset->getTargets());
$randomSplit = new StratifiedRandomSplit($dataset, 0.1);

// Build the model
$model    = new NaiveBayes();
$pipeline = new Pipeline(
    [
        new TokenCountVectorizer(new WordTokenizer()),
        new TfIdfTransformer(),
    ],
    $model
);

// Train the model using training data
$pipeline->train($randomSplit->getTrainSamples(), $randomSplit->getTrainLabels());

// Evaluate the model using testing data
$predictedLabels = $pipeline->predict($randomSplit->getTestSamples());
$accuracy        = Accuracy::score($randomSplit->getTestLabels(), $predictedLabels);
printf("Accuracy: %.1f%%\n", $accuracy * 100.0);

// Persist the model together with data transformers
$modelManager = new ModelManager();
$modelManager->saveToFile($pipeline, __DIR__.'/../model/pipeline');
