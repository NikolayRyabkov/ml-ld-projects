<?php

use Phpml\ModelManager;

ini_set('memory_limit', '-1');

include __DIR__.'/../vendor/autoload.php';

// Load trained model together with data transformers
$modelManager = new ModelManager();
$pipeline     = $modelManager->restoreFromFile(__DIR__.'/../model/pipeline');

// Retrieve text sample
while (empty($text)) $text = readline("Enter some text to analyze: \n");

// Predict text language
$predicted = $pipeline->predict([$text]);
printf("Predicted language: %s\n", $predicted[0]);
