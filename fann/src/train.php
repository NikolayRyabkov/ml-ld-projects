<?php

ini_set('memory_limit', '-1');

include __DIR__.'/../vendor/autoload.php';
include __DIR__.'/lib.php';

const DATASET_CSV_FILENAME  = __DIR__.'/../data/dataset.csv';
const DATASET_DATA_FILENAME = __DIR__.'/../data/dataset.data';
const TRAIN_FILENAME        = __DIR__.'/../data/train.data';
const TEST_FILENAME         = __DIR__.'/../data/test.data';

// Prepare data
$data = prepare_data_file(DATASET_CSV_FILENAME, DATASET_DATA_FILENAME);
split_data_file(DATASET_DATA_FILENAME, TRAIN_FILENAME, TEST_FILENAME, 0.1);

// Build the model [input layer size, hidden layer size, output layer size]
$layer_sizes = [$data['feature_count'], 5, $data['class_count']];
$model       = fann_create_standard_array(count($layer_sizes), $layer_sizes);
if ($model) {
    fann_set_training_algorithm($model, FANN_TRAIN_INCREMENTAL);
    fann_set_learning_rate($model, 0.01);
    fann_set_activation_function_hidden($model, FANN_SIGMOID_SYMMETRIC);
    fann_set_activation_function_output($model, FANN_SIGMOID_SYMMETRIC);
    fann_set_callback($model, 'generate_report');

    // Train the model using training data
    if (fann_train_on_file($model, TRAIN_FILENAME, 1000, 1, 0.001)) {

        // Evaluate model using testing data
        $test_dataset      = read_dataset_from_file(TEST_FILENAME);
        $predicted_outputs = predict($model, $test_dataset['inputs']);
        $accuracy          = calculate_accuracy($predicted_outputs, $test_dataset['outputs']);
        printf("Accuracy: %.1f%%\n", $accuracy * 100.0);

        // Persist model and data transformers
        fann_save($model, __DIR__.'/../model/model');
        file_put_contents(__DIR__.'/../model/classes', serialize($data['classes']));
        file_put_contents(__DIR__.'/../model/vectorizer', serialize($data['vectorizer']));
        file_put_contents(__DIR__.'/../model/transformer', serialize($data['transformer']));
    }
    fann_destroy($model);
}
