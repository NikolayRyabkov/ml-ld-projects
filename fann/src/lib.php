<?php

use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Extractors\CSV;
use Rubix\ML\Other\Tokenizers\Word;
use Rubix\ML\Transformers\TfIdfTransformer;
use Rubix\ML\Transformers\WordCountVectorizer;

function prepare_data_file(string $csv_filename, string $dataset_filename): array
{
    $dataset     = Labeled::fromIterator(new CSV($csv_filename, true));
    $classes     = array_values(array_unique($dataset->labels()));
    $vectorizer  = new WordCountVectorizer(10000, 1, PHP_INT_MAX, new Word());
    $transformer = new TfIdfTransformer();
    $dataset
        ->apply($vectorizer)
        ->apply($transformer);

    $sample_count  = count($dataset->samples());
    $feature_count = count($dataset->sample(0));
    $class_count   = count($classes);
    $header_string = sprintf("%d %d %d\n", $sample_count, $feature_count, $class_count);
    file_put_contents($dataset_filename, $header_string);
    foreach ($dataset->samples() as $index => $sample) {
        // Input string
        $input_string = '';
        foreach ($sample as $item) {
            $input_string .= $item.' ';
        }
        $input_string .= PHP_EOL;
        file_put_contents($dataset_filename, $input_string, FILE_APPEND);

        // Output string
        $label         = $dataset->labels()[$index];
        $output_string = '';
        foreach ($classes as $class) {
            $output_string .= $label === $class ? '1 ' : '0 ';
        }
        $output_string .= PHP_EOL;
        file_put_contents($dataset_filename, $output_string, FILE_APPEND);
    }

    return [
        'feature_count' => $feature_count,
        'class_count'   => $class_count,
        'classes'       => $classes,
        'vectorizer'    => $vectorizer,
        'transformer'   => $transformer,
    ];
}

function split_data_file(string $dataset_filename, string $train_filename, string $test_filename, float $split_ratio): void
{
    $dataset = fann_read_train_from_file($dataset_filename);

    // Calculate how many samples are in the test subset
    $dataset_size = fann_length_train_data($dataset);
    $test_size    = (int) floor($dataset_size * $split_ratio);

    // Split the subsets
    $test_data  = fann_subset_train_data($dataset, 0, $test_size);
    $train_data = fann_subset_train_data($dataset, $test_size, $dataset_size - $test_size);

    // Save the subsets to separate files
    fann_save_train($test_data, $test_filename);
    fann_save_train($train_data, $train_filename);
}

function generate_report($model, $train_data, $max_epochs, $epochs_between_reports, $desired_error, $epochs): bool
{
    printf("Epoch %d - Mean Squared Error: %f\n", $epochs, fann_get_MSE($model));

    return true;
}

function read_dataset_from_file(string $filename): array
{
    $dataset             = ['inputs' => [], 'outputs'  => []];
    $fh                  = fopen($filename, 'r');
    $header_str          = fgets($fh);
    list($dataset_size,) = explode(' ', $header_str);
    for ($i = 0; $i < (int) $dataset_size; $i++) {
        $inputs               = explode(' ', trim(fgets($fh)));
        $outputs              = explode(' ', trim(fgets($fh)));
        $dataset['inputs'][]  = array_map('floatval', $inputs);
        $dataset['outputs'][] = array_map('intval', $outputs);
    }

    return $dataset;
}

function predict($model, array $input_vectors): array
{
    $output_vectors = [];
    foreach ($input_vectors as $input_vector) {
        $output_vectors[] = round_output(fann_run($model, $input_vector));
    }

    return $output_vectors;
}

function calculate_accuracy(array $predictions, array $outputs): float
{
    $true_positive_count = 0;
    $total_count         = 0;
    foreach ($predictions as $index => $prediction) {
        if ($prediction === $outputs[$index]) {
            $true_positive_count++;
        }
        $total_count++;
    }

    return $true_positive_count / $total_count;
}

function round_output(array $predicted_output): array
{
    $max_index                  = array_keys($predicted_output, max($predicted_output))[0];
    $rounded_output             = array_fill(0, count($predicted_output), 0);
    $rounded_output[$max_index] = 1;

    return $rounded_output;
}

function convert_text_to_input_vector(string $text): array
{
    $vectorizer  = unserialize(file_get_contents(__DIR__.'/../model/vectorizer'));
    $transformer = unserialize(file_get_contents(__DIR__.'/../model/transformer'));
    $dataset     = Unlabeled::build([$text]);
    $dataset
        ->apply($vectorizer)
        ->apply($transformer);

    return $dataset->sample(0);
}

function convert_output_vector_to_class(array $output_vector): ?string
{
    $classes = unserialize(file_get_contents(__DIR__.'/../model/classes'));
    $index   = array_search(1, $output_vector);

    return $classes[$index] ?? null;
}
