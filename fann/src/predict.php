<?php

ini_set('memory_limit', '-1');

include __DIR__.'/../vendor/autoload.php';
include __DIR__.'/lib.php';

// Load trained model together with data transformers
$model = fann_create_from_file(__DIR__.'/../model/model');
if ($model) {
    // Retrieve text sample
    while (empty($text)) $text = readline("Enter some text to analyze: \n");

    // Load and apply data transformers
    $input_vector = convert_text_to_input_vector($text);

    // Predict text language
    $output_vectors = predict($model, [$input_vector]);
    printf("Predicted language: %s\n", convert_output_vector_to_class($output_vectors[0]));

    fann_destroy($model);
}
